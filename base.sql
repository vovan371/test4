-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 20 2018 г., 03:49
-- Версия сервера: 5.5.45
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `base`
--

-- --------------------------------------------------------

--
-- Структура таблицы `test1`
--

CREATE TABLE IF NOT EXISTS `test1` (
  `id` int(123) NOT NULL AUTO_INCREMENT,
  `name` varchar(123) NOT NULL,
  `price` varchar(123) NOT NULL,
  `SKU` varchar(123) NOT NULL,
  `dvd_size` varchar(123) NOT NULL,
  `book_weight` varchar(123) NOT NULL,
  `furniture_type` varchar(123) NOT NULL,
  `height` varchar(123) NOT NULL,
  `width` varchar(133) NOT NULL,
  `length` varchar(123) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `test1`
--

INSERT INTO `test1` (`id`, `name`, `price`, `SKU`, `dvd_size`, `book_weight`, `furniture_type`, `height`, `width`, `length`) VALUES
(1, 'English Course. Beginners', '25 $', 'JVS20124', '2 X 700MB', '0', '0', '', '', ''),
(2, 'English Course. Pro', '40 $', 'JVS20126', '4 X 700MB', '0', '0', '', '', ''),
(3, 'Harry Potter. Pocket Edition.', '5 $', 'Bb123', '4 X 700MB', '0.5kg', '0', '', '', ''),
(4, 'English Course. Starters', '20 $', 'JVS20123', '700MB', '0', '0', '', '', ''),
(5, 'Harry Potter. Delux Edition.', '25 $', 'Bb126', '0', '1.8kg', '0', '', '', ''),
(6, 'Living Room', '25 $', 'TR1210', '0', '0', 'Living', '0.7m', '1.2', '1m'),
(7, 'Dining Room', '27 $', 'TR1211', '0', '0', 'Dining', '1.2m', '1m', '2m'),
(8, 'Harry Potter. Gift Edition.', '15 $', 'Bb125', '0', '1.5kg', '0', '', '', ''),
(9, 'Harry Potter. Table Edition.', '10 $', 'Bb124', '0', '1kg', '0', '', '', ''),
(10, 'Elegant', '30 $', 'TR1208', '0', '0', 'Elegant', '0.5m', '0.7 m', '1m');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
