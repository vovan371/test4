

<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `base`
//

// `base`.`test1`
$test1 = array(
  array('id' => '1','name' => 'English Course. Beginners','price' => '25 $','SKU' => 'JVS20124','dvd_size' => '2 X 700MB','book_weight' => '0','furniture_type' => '0','height' => '','width' => '','length' => ''),
  array('id' => '2','name' => 'English Course. Pro','price' => '40 $','SKU' => 'JVS20126','dvd_size' => '4 X 700MB','book_weight' => '0','furniture_type' => '0','height' => '','width' => '','length' => ''),
  array('id' => '3','name' => 'Harry Potter. Pocket Edition.','price' => '5 $','SKU' => 'Bb123','dvd_size' => '4 X 700MB','book_weight' => '0.5kg','furniture_type' => '0','height' => '','width' => '','length' => ''),
  array('id' => '4','name' => 'English Course. Starters','price' => '20 $','SKU' => 'JVS20123','dvd_size' => '700MB','book_weight' => '0','furniture_type' => '0','height' => '','width' => '','length' => ''),
  array('id' => '5','name' => 'Harry Potter. Delux Edition.','price' => '25 $','SKU' => 'Bb126','dvd_size' => '0','book_weight' => '1.8kg','furniture_type' => '0','height' => '','width' => '','length' => ''),
  array('id' => '6','name' => 'Living Room','price' => '25 $','SKU' => 'TR1210','dvd_size' => '0','book_weight' => '0','furniture_type' => 'Living','height' => '0.7m','width' => '1.2','length' => '1m'),
  array('id' => '7','name' => 'Dining Room','price' => '27 $','SKU' => 'TR1211','dvd_size' => '0','book_weight' => '0','furniture_type' => 'Dining','height' => '1.2m','width' => '1m','length' => '2m'),
  array('id' => '8','name' => 'Harry Potter. Gift Edition.','price' => '15 $','SKU' => 'Bb125','dvd_size' => '0','book_weight' => '1.5kg','furniture_type' => '0','height' => '','width' => '','length' => ''),
  array('id' => '9','name' => 'Harry Potter. Table Edition.','price' => '10 $','SKU' => 'Bb124','dvd_size' => '0','book_weight' => '1kg','furniture_type' => '0','height' => '','width' => '','length' => '')
);

?>
