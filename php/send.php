<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname  = "base";

if(isset($_POST['order']))
{
    try {

        // connect to mysql

        $pdoConnect = new PDO( "mysql:host=$servername;dbname=$dbname", $username, $password );
        echo "Connected";
    } catch (PDOException $exc) {
        echo $exc->getMessage();
        exit();
    }

    // get values form 
    $name = $_POST['name'];
    $price = $_POST['price'];
    $SKU = $_POST['SKU'];
    $dvd = $_POST['choice_dvd'];
    $book= $_POST['choice_book'];
    $furniture = $_POST['choice_furniture'];
    $height = $_POST['choice_height'];
    $width = $_POST['width_choice'];
    $length = $_POST['length_choice'];




    
    // mysql query to insert data

    $pdoQuery = "INSERT INTO `test1`(`name`, `price`, `SKU`, `dvd_size`, `book_weight`,
    `furniture_type`, `height`, `width`, `length`) 
    VALUES (:name,:price,:SKU,:choice_dvd,:choice_book,:choice_furniture,:choice_height,:width_choice,:length_choice)";
    
    $pdoResult = $pdoConnect->prepare($pdoQuery);
    
    $pdoExec = $pdoResult->execute(array(":name"=>$name,":price"=>$price,":SKU"=>$SKU,":choice_dvd"=>$dvd,":choice_book"=>$book,
    ":choice_furniture"=>$furniture,":choice_height"=>$height,":width_choice"=>$width,":length_choice"=>$length));

    
    // check if mysql insert query successful
    if($pdoExec)
    {
        echo 'Data Inserted';
    }else{
        echo 'Data Not Inserted';
    }
}

?>
