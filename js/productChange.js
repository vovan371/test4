// Change product type 

function ProductChange(e) {
  var label = e.value;
  if (label == 1) {
    document.getElementById("product__1").style.display = 'block';
    document.getElementById("product__2").style.display = 'none';
    document.getElementById("product__3").style.display = 'none';
  } else if (label == 2) {
    document.getElementById("product__1").style.display = 'none';
    document.getElementById("product__2").style.display = 'block';
    document.getElementById("product__3").style.display = 'none';


  } else if (label == 3) {
    document.getElementById("product__1").style.display = 'none';
    document.getElementById("product__2").style.display = 'none';
    document.getElementById("product__3").style.display = 'block';

  } else {
    document.getElementById("product__1").style.display = 'none';
    document.getElementById("product__2").style.display = 'none';
    document.getElementById("product__3").style.display = 'none';

  }
}
