$(document).ready(function () {
    $('#books').on('change', function () {
        name = $('#books option:selected').attr('data-name');
        price = $('#books option:selected').attr('data-price');
        SKU = $('#books option:selected').attr('data-delivery');

        $('#name').empty().append(name);
        $('#price').empty().append(price);
        $('#SKU').empty().append(SKU);

        $('textarea[name="product-name"]').val(name);
        $('textarea[name="product-price"]').val(price);
        $('textarea[name="product-delivery"]').val(SKU);
    });

});



function BooksChange() {
    document.getElementById('pictures').innerHTML = document.getElementById(document.getElementById('books').value).innerHTML
}